# Field Switch File System

Field Switch File System - switch files from public to private and vice versa.


## Install

* Enable the module admin/modules or drush en field_switch_file_system.
* Using the module

## Using the module

* On the configuration page (admin/config/system/switch-file-system), you can choose one field from list of available fields.
* After you have selected a field, you will see current file system of the field and how many content types are using it.
* File system migration is selected automatically in depending on current file system of the field.


## Warning

* Backup your database and files before starting migration.
